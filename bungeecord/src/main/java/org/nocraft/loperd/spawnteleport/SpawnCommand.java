package org.nocraft.loperd.spawnteleport;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class SpawnCommand extends Command {
    private final SpawnTeleportPluginBungee plugin;

    public SpawnCommand(SpawnTeleportPluginBungee plugin) {
        super("spawn");
        this.plugin = plugin;
    }

    @SuppressWarnings("UnstableApiUsage")
    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length > 0) {
            sender.sendMessage(Message.TOO_MANY_ARGUMENTS.asComponent());
            return;
        }

        ProxiedPlayer player = (ProxiedPlayer) sender;

        ServerInfo server = plugin.getSpawnServer();
        if (server.equals(player.getServer().getInfo())) {
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF("Teleport");
            out.writeUTF(player.getName());
            server.sendData(SpawnTeleportChannels.TP_CHANNEL,
                    out.toByteArray());
            return;
        }

        player.connect(server);
    }
}
