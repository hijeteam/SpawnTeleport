package org.nocraft.loperd.spawnteleport;

import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Plugin;

public final class SpawnTeleportPluginBungee extends Plugin {

    private ServerInfo spawnServer;
    private SpawnCommand spawnCommand;

    @Override
    public void onEnable() {
        this.spawnCommand = new SpawnCommand(this);

        getProxy().getPluginManager().registerCommand(this, spawnCommand);
        getProxy().registerChannel(SpawnTeleportChannels.TP_CHANNEL);

        ServerInfo server = getProxy().getServerInfo("spawn");

        if (server == null) {
            this.onDisable();
            return;
        }

        this.spawnServer = server;
    }

    public ServerInfo getSpawnServer() {
        return this.spawnServer;
    }

    public void onDisable() {
        getProxy().getPluginManager().unregisterCommand(this.spawnCommand);
    }
}
